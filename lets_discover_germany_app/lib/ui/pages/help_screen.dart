import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_image/firebase_image.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';

class HelpScreen extends StatefulWidget {
  @override
  _HelpScreenState createState() => _HelpScreenState();

  final String serviceCategory;

  HelpScreen({Key key, @required this.serviceCategory}) : super(key: key);
}

class _HelpScreenState extends State<HelpScreen> {
  get serviceCategory => widget.serviceCategory;

  @override
  void initState() {
    super.initState();
  }

  //@override
  Widget build1(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Test"),
      ),
      body: Image(
        image: FirebaseImage('gs://discover-germany.appspot.com/Berlin.png',
            shouldCache: true, // The image should be cached (default: True)
            maxSizeBytes: 3000 * 1000, // 3MB max file size (default: 2.5MB)
            cacheRefreshStrategy:
                CacheRefreshStrategy.NEVER // Switch off update checking
            ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: FirebaseFirestore.instance
          .collection('service_category')
          .doc(serviceCategory)
          .collection("children")
          .snapshots(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) return LinearProgressIndicator();

        return _buildList(context, snapshot.data.docs);
      },
    );
  }

  _buildList(BuildContext context, List<DocumentSnapshot> snapshot) {
    return GridView.count(
      primary: false,
      padding: const EdgeInsets.all(20),
      crossAxisSpacing: 10,
      mainAxisSpacing: 10,
      crossAxisCount: 4,
      children: snapshot.map((data) => _buildListItem(context, data)).toList(),
    );
  }

  Widget _buildListItem(BuildContext context, DocumentSnapshot data) {
    return new Text("", style: new TextStyle(color: Colors.black));
  }
}
