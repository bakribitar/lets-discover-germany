import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:letsdiscovergermany/core/model/serviceCategory_model.dart';
import 'package:letsdiscovergermany/core/service/api.dart';
import 'package:letsdiscovergermany/ui/pages/categoryItems/categoryItemsList_widget.dart';

import '../../../app_theme.dart';
import '../../../constants.dart';
import 'package:firebase_core/firebase_core.dart';


// ignore: must_be_immutable
class MainListWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MainListWidgetState();
  }
}

class MainListWidgetState extends State<MainListWidget> {
  List<ServiceCategoryModel> categoriesModel;
  List<ServiceCategoryModel> searchResult;
  bool initialized = false;

  @override
  Widget build(BuildContext context) {


    return SingleChildScrollView(
        child: Column(children: <Widget>[
      _searchBar(context),
      if (initialized && searchResult !=null)
        GridView.builder(
            itemCount: searchResult.length,
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            primary: false,
            gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2),
            itemBuilder: (BuildContext context, int index) {
              return _categoryDetailsCard(context, searchResult[index]);
            })
      else
        StreamBuilder<QuerySnapshot>(
            stream:
            FirebaseFirestore.instance.collection('service_category').snapshots(),
            builder:
                (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
              if (snapshot.hasError) {
                return new Text('Error: ${snapshot.error}');
              }

              switch (snapshot.connectionState) {
                case ConnectionState.none:
                  return new Text('Not connected to the Stream or null');

                case ConnectionState.waiting:
                  return new Text('Awaiting for interaction');

                case ConnectionState.active:
                  var totalCount = 0;
                  List<DocumentSnapshot> categories;

                  if (snapshot.hasData) {
                    categories = snapshot.data.docs;
                    totalCount = categories.length;
                    categoriesModel = categories
                        .map((e) =>
                            ServiceCategoryModel.fromMap(e.data(), e.id))
                        .toList();

                    if (totalCount > 0) {
                      initialized = true;
                      return GridView.builder(
                          itemCount: totalCount,
                          scrollDirection: Axis.vertical,
                          shrinkWrap: true,
                          primary: false,
                          gridDelegate:
                              new SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 2),
                          itemBuilder: (BuildContext context, int index) {
                            return _categoryDetailsCard(
                                context, categoriesModel[index]);
                          });
                    }
                  }

                  return new Center(
                      child: Column(
                    children: <Widget>[
                      new Padding(
                        padding: const EdgeInsets.all(10.0),
                      ),
                      new Text(
                        "لا يوجد اتصال.",
                        style: Theme.of(context).textTheme.bodyText1,
                      )
                    ],
                  ));
              }

              return Container(
                child: new Text(NO_RESULTS_FOUND),
              );
            }),
    ]));
  }

  Widget _categoryDetailsCard(
      BuildContext context, ServiceCategoryModel categoryModel) {
    return Card(
      child: InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            Navigator.push(
                context,
                new MaterialPageRoute(
                    builder: (context) =>
                        new CategoryItemsListPage(categoryModel)));
          },
          child: Stack(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  image: DecorationImage(
                      image: AssetImage(IMAGES_FOLDER_PATH +
                          categoryModel.imagePath +
                          ".png"),
                      fit: BoxFit.cover),
                ),
              ),
              Center(
                child: ClipRect(
                  child: BackdropFilter(
                    filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
                    child: Center(
                      widthFactor: 1,
                      heightFactor: 1,
                      child: Text(categoryModel.name,
                          textAlign: TextAlign.center,
                          maxLines: 2,
                          style: AppTheme.textStyleCategory),
                    ),
                  ),
                ),
              )
            ],
          )),
    );
  }

  Widget _searchBar(BuildContext context) {
    return Container(
      alignment: Alignment.topCenter,
      padding: EdgeInsets.all(5),
      child: TextField(
        style: AppTheme.textStyleTitle,
        autofocus: false,
        decoration: InputDecoration(
          prefixIcon: Icon(Icons.search),
          hintText: 'بحث',
        ),
        textAlign: TextAlign.right,
        onChanged: (value) {
          setState(() {
            if (value != null && value.isNotEmpty)
              searchResult =
                  categoriesModel.where((element) => element.name.contains(value.trim())).toList();
            else
              searchResult = categoriesModel.toList();
          });
        },
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
      ),
    );
  }
}
