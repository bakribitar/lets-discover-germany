import 'dart:developer';
import 'dart:ui';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:firebase_image/firebase_image.dart';
import 'package:flutter/material.dart';
import 'package:letsdiscovergermany/core/model/service_model.dart';
import 'package:letsdiscovergermany/ui/pages/serviceDetails/fullImageSlider_widget.dart';
import 'package:letsdiscovergermany/ui/pages/serviceDetails/detailsTab_widget.dart';

import '../../../app_theme.dart';

// ignore: must_be_immutable
class ServiceDetailsPage extends StatelessWidget {
  ServiceDetailsPage(this.serviceModel);

  final ServiceModel serviceModel;
  double screenWidth;
  double screenHeight;

  @override
  Widget build(BuildContext context) {
    screenWidth = MediaQuery.of(context).size.width;
    screenHeight = MediaQuery.of(context).size.height;
    List<String> imagePaths = serviceModel.safeGetImagePaths();


    return Directionality(
        // add this
        textDirection: TextDirection.rtl,
        child: Scaffold(
            appBar: AppBar(
              title: Text(
                serviceModel.name,
                style: AppTheme.textStyleSubtitleWhite,
              ),
            ),
            body: SingleChildScrollView(

                child: new Column(children: <Widget>[
              CarouselSlider(
                options: CarouselOptions(
                  enableInfiniteScroll: imagePaths.length>1,
                  autoPlay: imagePaths.length>1,
                  autoPlayInterval: Duration(seconds: 2),
                  autoPlayAnimationDuration: Duration(milliseconds: 500),
                  pauseAutoPlayOnTouch: true,
                  enlargeCenterPage: true,
                ),

                    items: imagePaths.map((i) {
                  return InkWell(onTap: () {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => new Directionality(
                                textDirection: TextDirection.rtl,
                                child: FullImageSlider(
                                    imagePaths, i))));
                  }, child: Builder(
                    builder: (BuildContext context) {
                      return Container(
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.symmetric(
                            vertical: 5.0, horizontal: 5.0),
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              image: Image(
                                image: FirebaseImage(i,
                                    shouldCache: true,
                                    maxSizeBytes: 3000 * 1000,
                                    cacheRefreshStrategy:
                                        CacheRefreshStrategy.NEVER),
                              ).image,
                              fit: BoxFit.cover),
                        ),
                      );
                    },
                  ));
                }).toList(),
              ),
                    Container(
                  height:MediaQuery.of(context).size.height * 0.9,
                child: ServiceDetailsTab(serviceModel),
              )
            ]))));
  }
}
