import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:letsdiscovergermany/app_theme.dart';
import 'package:letsdiscovergermany/core/model/service_model.dart';
import 'package:maps_launcher/maps_launcher.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:letsdiscovergermany/ui/pages/serviceDetails/qrImgae_widget.dart';
import 'package:letsdiscovergermany/ui/module_widgets/webView_widget.dart';
import 'package:letsdiscovergermany/ui/module_widgets/alertDialog_widget.dart';

import '../../../constants.dart';

// ignore: must_be_immutable
class ServiceDetailsTab extends StatefulWidget {
  ServiceDetailsTab(this.serviceModel);
  ServiceModel serviceModel;

  @override
  State<StatefulWidget> createState() {
    return TabsState(this.serviceModel);
  }
}

class TabsState extends State<ServiceDetailsTab> {
  ServiceModel serviceModel;
  TabsState(this.serviceModel);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Padding(
        padding: const EdgeInsets.all(2.0),
        child: Directionality(
            textDirection: TextDirection.rtl,
            child: Scaffold(
                appBar: new AppBar(
                  automaticallyImplyLeading: false,
                  flexibleSpace: new Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      new TabBar(
                        tabs: [
                          new Tab(
                              child: Text(
                            "العنوان ورقم الهاتف",
                            textAlign: TextAlign.center,
                            style: AppTheme.textStyleSubtitleWhite,
                          )),
                          new Tab(
                              child: Text(
                            "معلومات عامة",
                            textAlign: TextAlign.center,
                            style: AppTheme.textStyleSubtitleWhite,
                          )),
                          new Tab(
                              child: Text(
                            "تقييمات المستخدمين",
                            textAlign: TextAlign.center,
                            style: AppTheme.textStyleSubtitleWhite,
                          )),
                        ],
                      )
                    ],
                  ),
                ),
                body: TabBarView(
                  children: [
                    _getAddressWidget(context, serviceModel),
                    _getInfoWidget(serviceModel),
                    Image.asset(
                      COMING_SOON_IMAGE_PATH,
                      fit: BoxFit.fitWidth,
                    )
                  ],
                ))),
      ),
    );
  }
}

_getInfoWidget(ServiceModel serviceModel) {
  return SingleChildScrollView(

      child: Column(children: <Widget>[
        if (serviceModel.workTimes.isNotEmpty)
          ListTile(
            leading: Ink(
                decoration: const ShapeDecoration(
                  color: AppTheme.mainAccentColor,
                  shape: CircleBorder(),
                ),
                child: IconButton(
                    onPressed: () => _showWorkTimes(serviceModel),
                    icon: Icon(
                      Icons.access_time,
                      color: Colors.white,
                    ))),
            title: Text(
              "أوقات العمل",
              style: AppTheme.textStyleCategory,
            ),
            subtitle: Text(
              serviceModel.workTimes,
              style: AppTheme.detailsInfoTextStyle,
            ),
          ),
        if (serviceModel.desc.isNotEmpty)
          RichText(
            textAlign: TextAlign.right,
            text: TextSpan(
              text: serviceModel.desc,
              style: AppTheme.textStyleTitle,
            ),
          )

      ]));
}

//TODO: refactor in own file/state
_getAddressWidget(BuildContext context, ServiceModel serviceModel) {
  return ListView(
    primary: false,
    children: <Widget>[
      ListTile(
        leading: Ink(
            decoration: const ShapeDecoration(
              color: AppTheme.mainAccentColor,
              shape: CircleBorder(),
            ),
            child: IconButton(
                onPressed: () => _openMaps(context, serviceModel),
                icon: Icon(
                  Icons.location_on,
                  color: Colors.white,
                ))),
        title: Text(
          "العنوان:",
          style: AppTheme.textStyleCategory,
        ),
        subtitle: Text(
          serviceModel.getAddressAsString(),
          style: AppTheme.detailsInfoTextStyle,
        ),
      ),
      if (serviceModel.phone.isNotEmpty || serviceModel.mob.isNotEmpty)
        ListTile(
          isThreeLine:
              serviceModel.phone.isNotEmpty && serviceModel.mob.isNotEmpty,
          leading: Ink(
              decoration: const ShapeDecoration(
                color: AppTheme.mainAccentColor,
                shape: CircleBorder(),
              ),
              child: IconButton(
                  onPressed: () => _showCallConfirm(context, serviceModel),
                  icon: Icon(
                    Icons.phone,
                    color: Colors.white,
                  ))),
          title: Text(
            "رقم الهاتف:",
            style: AppTheme.textStyleCategory,
          ),
          subtitle: Text(
            serviceModel.mob.isNotEmpty
                ? (serviceModel.mob + '\n' + serviceModel.phone)
                : serviceModel.phone,
            style: AppTheme.detailsInfoTextStyle,
          ),
        ),
      ListTile(
        leading: Ink(
            decoration: const ShapeDecoration(
              color: AppTheme.mainAccentColor,
              shape: CircleBorder(),
            ),
            child: IconButton(
                onPressed: () => _createShareMenu(serviceModel),
                icon: Icon(
                  Icons.share,
                  color: Colors.white,
                ))),
        title: Text(
          "مشاركة وإرسال لصديق",
          style: AppTheme.textStyleCategory,
        ),
        subtitle: FlatButton(
          child: Text(
            "اضغط للمشاركة",
            style: AppTheme.detailsInfoTextStyle,
          ),
          onPressed: () => {_createShareMenu(serviceModel)},
        ),
        trailing: IconButton(
            onPressed: () => _openQr(context, serviceModel),
            icon: Icon(
              Icons.calendar_view_day,
              color: AppTheme.mainAccentColor,
            )),
      ),
      if (serviceModel.web.isNotEmpty)
        ListTile(
          leading: Ink(
              decoration: const ShapeDecoration(
                color: AppTheme.mainAccentColor,
                shape: CircleBorder(),
              ),
              child: IconButton(
                  onPressed: () => _launchURL(context, serviceModel),
                  icon: Icon(
                    Icons.computer,
                    color: Colors.white,
                  ))),
          title: Text(
            "الموقع الالكتروني",
            style: AppTheme.textStyleCategory,
          ),
          subtitle: SelectableText(
            serviceModel.web,
            style: AppTheme.detailsInfoTextStyle,
          ),
        )
    ],
  );
}

_openQr(BuildContext context, ServiceModel serviceModel) {
  Navigator.push(context,
      new MaterialPageRoute(builder: (context) => QrImageWidget(serviceModel)));
}

_createShareMenu(ServiceModel serviceModel) {
  Share.share(
    "تطبيق لنكتشف ألمانيا" +
        GOOGLE_PLAY_APP_LINK +
        "\n" +
        serviceModel.name +
        "\n" +
        serviceModel.getAddressAsString(),
    // subject: "subject",
  );
}

_launchURL(BuildContext context, ServiceModel serviceModel) async {
  String url = "https://" + serviceModel.web;
  String siteOf = "الموقع الخاص بـ ";
  if (await canLaunch(url))
    Navigator.of(context).push(MaterialPageRoute(
        builder: (BuildContext context) => MyWebView(
              title: siteOf + serviceModel.name,
              selectedUrl: url,
            )));
  else {
    Fluttertoast.showToast(
      msg: "الموقع غير متاح ضمن التطبيق. قم بنسخه ولصقه في المتصفح",
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.CENTER,
    );
  }
}

_showWorkTimes(ServiceModel serviceModel) async {}

_showCallConfirm(BuildContext context, ServiceModel serviceModel) {
  String number =
      serviceModel.mob.isNotEmpty ? serviceModel.mob : serviceModel.phone;
  VoidCallback continueCallBack =
      () => {launch("tel://" + number), Navigator.of(context).pop()};
  BlurryDialog alert = BlurryDialog("سيتم الإتصال",
      "هل تريد الإتصال بهذا الرقم؟" + "\n" + number, continueCallBack);

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

_openMaps(BuildContext context, ServiceModel serviceModel) {
  VoidCallback continueCallBack =
      () => {MapsLauncher.launchQuery(serviceModel.getAddressAsString())};
  BlurryDialog alert = BlurryDialog(
      "فتح الخرائط", "هل تريد فتح العنوان في الخرائط؟", continueCallBack);

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
