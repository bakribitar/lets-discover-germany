import 'package:flutter/material.dart';
import 'package:letsdiscovergermany/core/model/service_model.dart';
import 'package:qr_flutter/qr_flutter.dart';

import '../../../app_theme.dart';
import '../../../constants.dart';

// ignore: must_be_immutable
class QrImageWidget extends StatelessWidget {
  ServiceModel serviceModel;
  QrImageWidget(this.serviceModel);

  @override
  Widget build(BuildContext context) {
    return Directionality(
        textDirection: TextDirection.rtl,
        child: Scaffold(
            appBar: AppBar(
              title: Text(
                "مشاركة عن طريق الكود",
                style: AppTheme.textStyleSubtitleWhite,
              ),
            ),
            body: Center( child:
              QrImage(
                data: serviceModel.getAddressAsString() +
                    "\n\n" +
                    GOOGLE_PLAY_APP_LINK,
                foregroundColor: AppTheme.mainColor,
                version: QrVersions.auto,
                size: 350.0,
              )
             )));
  }
}
