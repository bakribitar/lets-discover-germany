
// ignore: must_be_immutable
import 'package:firebase_image/firebase_image.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class FullImageSlider extends StatefulWidget {
  @override
  FullImageSliderState createState() =>
      FullImageSliderState(this.imageUrls, this.selectedImage);
  List<String> imageUrls;
  String selectedImage;
  FullImageSlider(this.imageUrls, this.selectedImage);
}

class FullImageSliderState extends State<FullImageSlider> {
  PageController _controller;

  FullImageSliderState(this.imageUrls, this.selectedImage) {
    int index = this.imageUrls.indexOf(selectedImage);
    _controller = PageController(
      initialPage: index,
    );
  }
  String selectedImage;
  List<String> imageUrls;

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return PageView(controller: _controller, children: <Widget>[
      for (var url in imageUrls)
        (Center(child: Image(image: FirebaseImage(url))))
    ]);
  }
}
