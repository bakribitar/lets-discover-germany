import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_image/firebase_image.dart';
import 'package:flutter/material.dart';
import 'package:letsdiscovergermany/core/model/serviceCategory_model.dart';
import 'package:letsdiscovergermany/core/model/service_model.dart';
import 'package:letsdiscovergermany/ui/pages/serviceDetails/serviceDetails_widget.dart';
import 'dart:math' as math;
import '../../../app_theme.dart';
import '../../../constants.dart';

// ignore: must_be_immutable
class CategoryItemsListPage extends StatelessWidget {
  CategoryItemsListPage(this.category);

  final ServiceCategoryModel category;
  double screenWidth;
  double screenHeight;

  @override
  Widget build(BuildContext context) {
    String categoryId = category.id;

    screenWidth = MediaQuery.of(context).size.width;
    screenHeight = MediaQuery.of(context).size.height;

    String path = ServiceModel.getCollectionName();
    return Directionality(
        // add this
        textDirection: TextDirection.rtl,
        child: Scaffold(
            appBar: AppBar(
              title: Text(
                category.name,
                style: AppTheme.textStyleSubtitleWhite,
              ),
            ),
            body: StreamBuilder<QuerySnapshot>(
                stream: FirebaseFirestore.instance
                    .collection(path)
                    .where("categoryId", isEqualTo: categoryId)
                    .snapshots(),
                builder: (BuildContext context,
                    AsyncSnapshot<QuerySnapshot> snapshot) {
                  if (snapshot.hasError) {
                    return new Text('Error: ${snapshot.error}');
                  }

                  switch (snapshot.connectionState) {
                    case ConnectionState.none:
                      return new Text('لا يوجد اتصال.',
                          style: AppTheme.textStyleTitle);

                    case ConnectionState.waiting:
                      return Center(child: CircularProgressIndicator());

                    case ConnectionState.active:
                      var totalCount = 0;
                      List<DocumentSnapshot> categories;
                      List<ServiceModel> categoriesModel;

                      if (snapshot.hasData) {
                        categories = snapshot.data.docs;
                        totalCount = categories.length;
                        categoriesModel = categories
                            .map((e) =>
                                ServiceModel.fromMap(e.data(), e.id))
                            .toList();

                        if (totalCount > 0) {
                          return GridView.builder(
                              itemCount: totalCount,
                              scrollDirection: Axis.vertical,
                              shrinkWrap: true,
                              primary: false,
                              gridDelegate:
                                  new SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 3),
                              itemBuilder: (BuildContext context, int index) {
                                return _categoryDetailsCard(
                                    context, categoriesModel[index]);
                              });
                        }
                      }

                      return new Center(
                          child: Column(
                        children: <Widget>[
                          new Padding(
                            padding: const EdgeInsets.all(10.0),
                          ),
                          new Text(NO_RESULTS_FOUND,
                              style: AppTheme.textStyleTitle)
                        ],
                      ));
                  }

                  return Container(
                    child: new Text(
                      NO_RESULTS_FOUND,
                      style: AppTheme.textStyleTitle,
                    ),
                  );
                })));
  }

  Widget _categoryDetailsCard(BuildContext context, ServiceModel serviceModel) {
    return Card(
      child: InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            Navigator.push(
                context,
                new MaterialPageRoute(
                    builder: (context) =>
                        new ServiceDetailsPage(serviceModel)));
          },
          child: Stack(
            children: <Widget>[
              _getImageOrPlaceHolderColor(serviceModel),
              Center(
                  child: ClipRect(
                child: BackdropFilter(
                  filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
                  child: Center(
                      widthFactor: 1,
                      heightFactor: 1,
                      child: Padding(
                        padding: EdgeInsets.only(left: 10, right: 10),
                        child: Text(serviceModel.name,
                            textAlign: TextAlign.center,
                            maxLines: 2,
                            style: AppTheme.textStyleCategory),
                      )),
                ),
              )),
            ],
          )),
    );
  }
}

_getImageOrPlaceHolderColor(ServiceModel serviceModel) {
  if (serviceModel.imageCount != 0)
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        image: DecorationImage(
            image: Image(
              image: FirebaseImage(serviceModel.safeGetImagePaths()[0],
                  shouldCache:
                      true, // The image should be cached (default: True)
                  maxSizeBytes:
                      3000 * 1000, // 3MB max file size (default: 2.5MB)
                  cacheRefreshStrategy:
                      CacheRefreshStrategy.NEVER // Switch off update checking
                  ),
            ).image,
            fit: BoxFit.cover),
      ),
    );

  Color startColor =
      Color((math.Random().nextDouble() * 0xFFFFFF).toInt()).withOpacity(0.2);
  Color endColor =
      Color((math.Random().nextDouble() * 0xFFFFFF).toInt()).withOpacity(0.7);

  return Container(
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(15),
      gradient: LinearGradient(
        begin: Alignment.topRight,
        end: Alignment.bottomLeft,
        colors: [startColor, endColor],
        tileMode: TileMode.repeated, // repeats the gradient over the canvas
      ),
    ),
  );
}
