import 'package:flutter/material.dart';
import 'package:letsdiscovergermany/ui/pages/admin/admin_page.dart';

import '../../app_theme.dart';
import '../../constants.dart';

class AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          _createHeader(),
          if (SHOW_ADMIN_PAGE)
            _createDrawerItem(
              icon: Icons.event,
              text: 'لوحة المشرف',
              onTap: () => Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => AdminPage())),
            ),
          _createDrawerItem(
            icon: Icons.note,
            text: 'ملاحظات',
          ),
          Divider(),
          _createDrawerItem(icon: Icons.stars, text: 'روابط مفيدة'),
          Divider(),
          _createDrawerItem(icon: Icons.bug_report, text: 'أبلغ عن مشكلة'),
          ListTile(
            title: Text('0.0.1', style: AppTheme.textStyleSubtitle),
            onTap: () {},
          ),
        ],
      ),
    );
  }

  Widget _createHeader() {
    return DrawerHeader(
        margin: EdgeInsets.zero,
        padding: EdgeInsets.zero,
        decoration: BoxDecoration(
            image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage('assets/images/cover.png'))),
        child: Stack(children: <Widget>[
          Positioned(
              bottom: 12.0,
              right: 16.0,
              child: Text("لنكتشف ألمانيا!", style: AppTheme.textStyleTitle)),
        ]));
  }

  Widget _createDrawerItem(
      {IconData icon, String text, GestureTapCallback onTap}) {
    return ListTile(
      title: Row(
        children: <Widget>[
          Icon(icon, color: AppTheme.mainAccentColor),
          Padding(
            padding: EdgeInsets.only(right: 8.0),
            child: Text(
              text,
              style: AppTheme.textStyleSubtitle,
            ),
          )
        ],
      ),
      onTap: onTap,
    );
  }
}
