import 'dart:ui';
import 'package:flutter/material.dart';

import '../../app_theme.dart';


class BlurryDialog extends StatelessWidget {

  String title;
  String content;
  VoidCallback continueCallBack;

  BlurryDialog(this.title, this.content, this.continueCallBack);

  @override
  Widget build(BuildContext context) {
    return
      Directionality(
      textDirection: TextDirection.rtl,
      child: BackdropFilter(
      filter: ImageFilter.blur(sigmaX: 6, sigmaY: 6),
      child:  AlertDialog(
      title: new Text(title,style: AppTheme.textStyleTitle,),
      content: new Text(content, style: AppTheme.textStyleSubtitle,),
      actions: <Widget>[
        new FlatButton(
          child: new Text("اي نعم"),
           onPressed: () {
            continueCallBack();
          },
        ),
        new FlatButton(
          child: Text("إلغاء"),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ],
      )));
  }
}