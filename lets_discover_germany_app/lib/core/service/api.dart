import 'package:cloud_firestore/cloud_firestore.dart';

class Api {
  final FirebaseFirestore databaseReference = FirebaseFirestore.instance;
  String collectionPath;
  CollectionReference collectionReference;

  Api(collectionPath) {
    collectionReference = databaseReference.collection(collectionPath);
  }

  Future<QuerySnapshot> getDataCollectionDocuments() {
    return collectionReference.get();
  }

  Stream<QuerySnapshot> streamDataCollectionDocuments() {
    return collectionReference.snapshots();
  }

  Future<DocumentSnapshot> getDocumentById(String id) {
    return collectionReference.doc(id).get();
  }

  Future<void> removeDocument(String id) {
    return collectionReference.doc(id).delete();
  }

  Future<DocumentReference> addDocument(Map data) {
    return collectionReference.add(data);
  }

  Future<void> updateDocument(Map data, String id) {
    return collectionReference.doc(id).update(data);
  }
}
