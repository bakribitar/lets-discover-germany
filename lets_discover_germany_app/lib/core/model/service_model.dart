import '../../constants.dart';

class ServiceModel {
  static String getCollectionName() => "service";
  String id;
  String name;
  String categoryId;
 // List<String> imagePaths;
  String desc;
  int postalCode;
  String city;
  String houseNum;
  String str;
  String phone;
  String mob;
  String web;
  String workTimes;
  bool active;
  int imageCount;



  ServiceModel(
      {this.id,
      this.name,
      this.categoryId,
    //this.imagePaths,
      this.desc,
      this.postalCode,
      this.city,
      this.houseNum,
      this.str,
      this.phone,
      this.mob,
      this.web,
      this.workTimes,
      this.imageCount,
      });

  ServiceModel.fromMap(Map snapshot, String id)
      : id = id ?? '',
        name = snapshot['name'] ?? '',
        categoryId = snapshot['categoryId'],
  //    imagePaths = convertToList(snapshot['imagePaths']),
        desc = snapshot['desc'] ?? '',
        postalCode = snapshot['postalCode'],
        city = snapshot['city'] ?? '',
        houseNum = snapshot['houseNum'] ?? '',
        str = snapshot['str'] ?? '',
        phone = snapshot['phone'] ?? '',
        mob = snapshot['mob'] ?? '',
        web = snapshot['web'] ?? '',
        workTimes = snapshot['workTimes'] ?? '',
        active = snapshot['active']?? false,
        imageCount = snapshot['imageCount'] ?? 0;


  toJson() {
    return {
      "name": name,
      "categoryId":categoryId,
 //   "imagePaths": imagePaths,
      "desc": desc,
      "postalCode": postalCode,
      "city": city,
      "houseNum": houseNum,
      "str": str,
      "phone": phone,
      "mob": mob,
      "web": web,
      "workTimes" : workTimes,
      "active" : active,
      "imageCount": imageCount
    };
  }

  List<String> safeGetImagePaths()
  {
    if(imageCount == 0)
      return new List.filled(1, COVER_IMAGE_FIREBASE);
    List<String> result = new List<String>();

    for(int i = 0 ; i< imageCount ;i++)
       result.add(GS_PATH_FIREBASE + categoryId + "/" +  id + "/" + i.toString() + ".jpg");

    return result;
  }
  String getAddressAsString() => "$str $houseNum, $postalCode $city";

  static int convertToInt(dynamic input) {
    if (input == null) return 0;
    return int.parse(input);
  }

  static List<String> convertToList(List<dynamic> input) {
    List<String> result = new List<String>();
    if (input == null) return result;
    input.forEach((element) {
      result.add(element.toString());
    });
    return result;
  }

}