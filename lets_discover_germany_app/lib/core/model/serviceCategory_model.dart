class ServiceCategoryModel {
  static String getCollectionName() => "service_category";
  String id;
  String name;
  String imagePath;

  ServiceCategoryModel({this.id, this.name, this.imagePath});

  ServiceCategoryModel.fromMap(Map snapshot, String id)
      : id = id ?? '',
        name = snapshot['name'] ?? '',
        imagePath = snapshot['imagePath'] ?? '';

  toJson() {
    return {
      "name": name,
      "imagePath": imagePath,
    };
  }
}
