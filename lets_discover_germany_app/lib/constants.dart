const String IMAGES_FOLDER_PATH = "assets/images/";
const String FALLBACK_IMAGE_NAME = "fallback.png";
const String COMING_SOON_IMAGE_PATH = IMAGES_FOLDER_PATH + "comingSoon.png";

const String COVER_IMAGE_FIREBASE = 'gs://discover-germany.appspot.com/cover.jpg';
const String GS_PATH_FIREBASE = 'gs://discover-germany.appspot.com/';
//TODO: change
const String GOOGLE_PLAY_APP_LINK = "https://play.google.com/store/apps/details?id=com.google.android.apps.translate";

const String BACK_TO_CATEGORIES = "العودة إلى التصنيفات";
const String NO_RESULTS_FOUND = "لم يتم العثور على أية نتائج.";

const bool SHOW_ADMIN_PAGE = false;