import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:letsdiscovergermany/core/model/serviceCategory_model.dart';
import 'package:letsdiscovergermany/ui/drawer/appDrawer_widget.dart';
import 'package:letsdiscovergermany/ui/pages/main/mainList_widget.dart';
import 'package:letsdiscovergermany/ui/pages/welcome/welcome_page.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'app_theme.dart';
import 'core/service/api.dart';

void main() async  {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Let\'s discover it!',
      theme: ThemeData.light().copyWith(
        primaryColor: AppTheme.mainColor,
        accentColor: AppTheme.mainAccentColor,

        // Define the default TextTheme. Use this to specify the default
        // text styling for headlines, titles, bodies of text, and more.
        textTheme: TextTheme(
          headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
          headline6: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
          bodyText2: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
        ),
      ),
      home: Directionality(
        // add this
        textDirection: TextDirection.rtl, // set this property
        child: HomePage(),
      ),
    );
  }
}

class HomePage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}


class HomePageState extends State<HomePage>
{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: AppDrawer(),
        appBar: AppBar(
          title: Text('تعرّف على ما يحيط بك!' ,style: AppTheme.textStyleSubtitleWhite,),
        ),
        body: MainListWidget());
  }
}