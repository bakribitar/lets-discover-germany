# Let's Discover Germany

Welcome to the **Let's Discover Germany** repository! This repository contains the source code and assets for a mobile application developed using Flutter and Dart. The app aims to provide users with a platform to explore and discover various attractions, landmarks, and destinations in Germany. It also utilizes Firebase services for authentication, data storage, and real-time updates.

## Features

- Explore Germany: Discover popular tourist attractions, landmarks, cities, and regions across Germany.
- Interactive Map: View an interactive map with markers indicating various points of interest.
- Detailed Information: Get detailed information about each location, including descriptions, ratings, reviews, and photos.
- Search and Filters: Find specific locations based on search queries or apply filters to refine the results.
- User Authentication: Sign up and log in securely using Firebase Authentication.
- Favorites: Save your favorite locations for quick access.
- Reviews and Ratings: Leave your own reviews and ratings for the places you visit.
- Real-time Updates: Receive real-time updates on new locations, reviews, and ratings.

## Tech Stack

The app is built using the following technologies:

- **Flutter**: A cross-platform framework for building native mobile applications.
- **Dart**: The programming language used to develop Flutter applications.
- **Firebase**: A comprehensive set of tools provided by Google to facilitate the development of web and mobile applications.

The specific Firebase services utilized in this project are:

- **Firebase Authentication**: Securely handle user authentication and authorization.
- **Cloud Firestore**: Store and retrieve data in a flexible and scalable NoSQL database.
- **Firebase Storage**: Efficiently store and serve user-generated content such as photos.
- **Firebase Realtime Database**: Enable real-time updates and notifications.

## Installation

To run the **Let's Discover Germany** app locally, follow these steps:

1. Clone the repository
2. Navigate to the project directory
3. Install the required dependencies using Flutter:    
`flutter pub get`
4. Set up Firebase:
	Create a new project on the Firebase console (https://console.firebase.google.com) if you haven't already.
	Enable Firebase Authentication, Cloud Firestore, Firebase Storage, and Firebase Realtime Database for your project.
	Download the google-services.json file for Android or the GoogleService-Info.plist file for iOS from the Firebase console.
	Place the downloaded file in the appropriate locations:
	For Android: android/app/google-services.json
	For iOS: ios/Runner/GoogleService-Info.plist
5. Run the app:
`flutter run`

## Contributing

We welcome contributions to enhance the **Let's Discover Germany** app! If you'd like to contribute, please follow these guidelines:

1. Fork the repository.
2. Create a new branch for your feature or bug fix.
3. Make your changes and test thoroughly.
4. Commit your changes with clear and descriptive messages.
5. Push your changes to your fork.
6. Submit a pull request to the `main` branch of the original repository.

Please ensure that your code adheres to the existing coding style and conventions.

## License

The **Let's Discover Germany** app is licensed under the [MIT License](LICENSE). Feel free to modify and distribute the app according to the terms of the license.

## Contact

If you have any questions or suggestions regarding the **Let's Discover Germany** app or this repository, please feel free to contact me at [bakri.bitar@protonmail.com](mailto:bakri.bitar@protonmail.com).

![gif](https://bitbucket.org/bakribitar/lets-discover-germany/raw/4f090a442d89f28de344b49fead8694c250ce759/demoImages/demo.gif)

![Screenshot1](https://bitbucket.org/bakribitar/lets-discover-germany/raw/4f090a442d89f28de344b49fead8694c250ce759/demoImages/screenshot%20(1).jpeg)

![Screenshot2](https://bitbucket.org/bakribitar/lets-discover-germany/raw/4f090a442d89f28de344b49fead8694c250ce759/demoImages/screenshot%20(2).jpeg)